Module Module1

    ' Set up the upper and lower bounds of the board
    Const MAX_X = 5
    Const MAX_Y = 5
    Const MIN_X = 0
    Const MIN_Y = 0

    ' This stores the cardinal directions
    Dim Compass(3) As String

    ' These are used to store the current position and direction of the robot
    Dim current_x As Integer
    Dim current_y As Integer
    Dim current_facing As Integer

    ' To store the status of the robot - placed or not placed - we start out not placed.
    Dim isPlaced = False


    ' Checks that the new position we want to apply is valid / allowed
    Function CheckNewPos(x, y)
        Dim isGood = True
        Dim errors = ""
        If x > MAX_X Or x < MIN_X Then
            isGood = False
            errors = errors & "X is not between " & MIN_X & " and " & MAX_X & vbCrLf
        End If
        If y > MAX_Y Or y < MIN_Y Then
            isGood = False
            errors = errors & "Y is not between " & MIN_Y & " and " & MAX_Y
        End If
        Return isGood
    End Function

    ' Get the coordinates of the position we're trying to move to.
    Function GetNewPos()
        Dim result(1)
        Select Case current_facing
            Case 0 ' north
                result(0) = current_x
                result(1) = current_y + 1
            Case 1 ' east
                result(0) = current_x + 1
                result(1) = current_y
            Case 2 ' south
                result(0) = current_x
                result(1) = current_y - 1
            Case 3 ' west
                result(0) = current_x - 1
                result(1) = current_y
        End Select
        Return result
    End Function


    Sub ProcCommand(comm)
        ' Set to lowercase for case insensitivity.
        comm = lCase(comm)
        ' If it looks like a correctly-formed Place command, try to process that.
        If Left(comm, 5) = "place" And Len(comm) > 5 And isPlaced = False Then
            comm = Replace(comm, "place", "")
            Dim args = Split(comm, ",")
            If UBound(args) <> 2 Then
                Console.WriteLine("Arguments for Place not correct, please fix.")
                Exit Sub
            End If
            PlaceRobot(args(0), args(1), args(2))

        Else
            ' It's something else, just go to regular processing.
            Select Case comm
                Case "report"
                    Console.WriteLine("X: " & current_x)
                    Console.WriteLine("Y: " & current_y)
                    Console.WriteLine("F: " & Compass(current_facing))
                Case "left"
                    current_facing = current_facing - 1
                    If current_facing < LBound(Compass) Then
                        current_facing = UBound(Compass)
                    End If
                    Console.WriteLine("Robot is now facing " & Compass(current_facing))
                Case "right"
                    current_facing = current_facing + 1
                    If current_facing > UBound(Compass) Then
                        current_facing = LBound(Compass)
                    End If
                    Console.WriteLine("Robot is now facing " & Compass(current_facing))
                Case "move"
                    Dim newpos = GetNewPos()
                    If CheckNewPos(newpos(0), newpos(1)) = True Then
                        ' We can move to the new coordinates
                        current_x = newpos(0)
                        current_y = newpos(1)
                        Console.WriteLine("Robot moved " & Compass(current_facing))
                    Else
                        ' The new coordinates are out-of-bounds
                        Console.WriteLine("Robot cannot go any further " & Compass(current_facing))
                    End If
                Case "place"
                    Console.WriteLine("Place robot where?")
                Case "turn"
                    Console.WriteLine("Which way?")
                Case "exit"
                    End
                Case Else
                    Console.WriteLine("Sorry, didn't recognise that command.")
                    Console.WriteLine("Please use Place, Left, Right, Move, or Report.")
            End Select

        End If

    End Sub


    Sub SetupCompass()
        Compass(0) = "north"
        Compass(1) = "east"
        Compass(2) = "south"
        Compass(3) = "west"
    End Sub

    Sub PlaceRobot(x, y, f)
        If CheckNewPos(x, y) = False Then
            ' The coordinates are out-of-bounds
            Console.WriteLine("You can't place the robot there.")
            Console.WriteLine("Please choose a valid starting position " & MIN_X & "-" & MAX_X & "," & MIN_Y & "-" & MAX_Y)
            Exit Sub
        End If
        Dim indx = Array.IndexOf(Compass, f)
        If indx = -1 Then
            ' The starting direction is invalid
            Console.WriteLine("That isn't a valid starting direction.")
            Exit Sub
        End If
        ' All good, set the coordinates and starting direction.
        current_x = x
        current_y = y
        current_facing = indx
        isPlaced = True
        Console.WriteLine("Robot now at: " & current_x & "," & current_y & "," & Compass(current_facing))
    End Sub


    Sub Main()
        SetupCompass()
        Dim comm
        Console.WriteLine("Ready! Please place the robot")
        Do
            comm = Console.ReadLine()
            If isPlaced = False And Left(comm, 5) <> "place" Then
                Console.WriteLine("Robot must be placed before issuing other commands.")
            Else
                ProcCommand(comm)
            End If

        Loop

    End Sub

End Module
